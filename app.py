import glob
import os

import cv2
import tensorflow as tf

from config import Configuration
from data_process.tf_record_reader import TFRecordReader
from data_process.tf_record_writer import TFRecordWriter
from models.evaluation import Evaluate
from models.model import VGGModel
from data_process.prepare_dataset import DataSet
from models.training import TrainModel
from utils.utility import Utility
from utils.visualization import Visualization


class EuroSatImageClassification:

    def __init__(self, config: Configuration):
        self.config = config

    def run_extract_data(self):
        """ This method is used for extracting the dataset and doing the train test split

        Returns: dataset

        """
        # initialize the dataset
        dataset = DataSet(config=self.config)

        # dataset.create_dir()
        # # This is for data_old zip file
        # dataset.extract_data(dataset.working_dir)
        # # This is for in case if there exists sub zip files(e.g train.zip)
        # dataset.extract_data(dataset.data_dir)
        # # Now Remove all files only keep train test directory
        # dataset.remove_files(dataset.data_dir, pattern='*.*')
        # # create train,test,validation directory
        # dirs = [dataset.test_dir, dataset.validation_dir, dataset.train_dir]
        # dataset.create_train_val_test_dir(dirs)

        return dataset

    def label_info(self):
        """ This method helps to print the label in the log

        Returns: None

        """
        print('\033[1mEncoded Label:\033[0m \n')
        for idx, label in enumerate(Utility.get_labels()):
            print('{}: {}'.format(idx, label))

    def plot_dataset(self):
        """ This method is used for plot the basic dataset

        Args:
            dataset: Dataset

        Returns:

        """
        train_img_dict = {}
        for label in Utility.get_labels():
            img_path = glob.glob(os.path.join(self.config.train_dir, label, '*'))[1]
            train_img_dict[label] = cv2.imread(img_path)

        Visualization.plot_dataset(train_img_dict=train_img_dict)

    def create_tf_records(self):
        """ This method is used for creating tf records

        Args:
            config: Configuration

        Returns: None

        """
        record_writer = TFRecordWriter(config=self.config)
        record_writer.create_record_dir()
        record_writer.write_data()

    def read_tf_records_test(self, record_visualize=False):
        """ Read TFRecords from record files

        Args:
            config: Configuration
            record_visualize: Boolean

        Returns: Dataset

        """

        reader = TFRecordReader(config=self.config)
        # Read and Parse data for training
        train_dataset = reader.generate_dataset(is_training=True)

        # Read and Parse data for validation
        validation_dataset = reader.generate_dataset(is_training=False)

        if record_visualize:
            Visualization.plot_tf_dataset(dataset=train_dataset)
            Visualization.plot_tf_dataset(dataset=validation_dataset)

        return train_dataset, validation_dataset

    def vgg_model(self):
        """ This function will create a VGGStyle Model
        and print the summary of the model

        Returns:

        """
        vgg_model = VGGModel(config=self.config)
        model = vgg_model.get_model()
        model.summary()
        return model

    def train_model(self, config: Configuration, model: tf.keras.Model,
                    train_dataset, validation_dataset):
        model_training = TrainModel(config=config)
        model_training.compile_model(model=model)
        model_training.train_model(model=model,
                                   train_dataset=train_dataset,
                                   validation_dataset=validation_dataset)

    def evaluation(self):
        """ This method is used for model evaluation on
        unknown test dataset

        Args:
            evaluate:

        Returns:

        """
        evaluate = Evaluate(config=self.config)
        x_test, y_test = evaluate.load_test_data()

        print(x_test.shape)
        print(y_test.shape)
        print(x_test.dtype)
        print(y_test.dtype)
        print("The dataset contains {} images".format(len(y_test)))

        # To check the dataset is not shuffled.
        print(y_test[:])

        # Plotting a validation image
        data = x_test[1] #random.randint(1, 20)
        Visualization.plot_numpy_image(img=data)

        # evaluate and plot confusion matrix
        prediction_class = evaluate.evaluate_model(x_test=x_test, y_test=y_test)
        Visualization.plot_confusion_matrix(y_test=y_test,
                                            prediction_class=prediction_class)


if __name__ == '__main__':
    config = Configuration()
    classification = EuroSatImageClassification(config=config)
    dataset = classification.run_extract_data()
    classification.label_info()

    # visualize the dataset
    classification.plot_dataset()

    # writing tf records
    classification.create_tf_records()

    # reading tf records
    train_dataset, validation_dataset = classification.read_tf_records_test(
        record_visualize=True)

    # Generate model
    model = classification.vgg_model()
    classification.train_model(config=config, model=model,
                               train_dataset=train_dataset,
                               validation_dataset=validation_dataset)

    # Evaluation
    classification.evaluation()
