# Eurosat Image Classification

EuroSAT: A land use and land cover classification dataset based on Sentinel-2 satellite images. The Sentinel-2 satellite images are openly and freely accessible provided in the Earth observation program Copernicus. The author present a novel dataset based on Sentinel-2 satellite images consisting out of 10 classes with in total 27,000 labeled and geo-referenced images The dataset is available [here](http://madm.dfki.de/files/sentinel/EuroSAT.zip)


#### Setup

Clone this repository by following command:

```
git clone https://gitlab.com/ankon111/eurosat_image_classification.git

```

#### Python Environment
This project is implemented in python. We recommend to use anaconda(miniconda) environment for Windows and Linux platforms.
To create new environment call:

```
conda env create --name envname --file=environment.yml

```

### Run
Put the dataset (the zip file) in the project directory. Open the `config.py` file and adjust necessary configurations e.g. train-test split ratio, `learning_rate` or `optimizers` etc. After completing the configuration setting run the following command from terminal.

```
source activate <environment_name>
python app.py
```

This command will execute following operation in sequential manner:


- Extract the zip file
- Process data and do train-test split
- Plot sample dataset
- Create TFRecords
- Read TFRecords
- Data augmentation on training data
- Create VGG Block model
- Train the model
- Save the model
- Evaluate the model


## References
<a id="1">[1]</a> 
[Eurosat: A novel dataset and deep learning benchmark for land use and land cover classification](https://arxiv.org/pdf/1709.00029.pdf), Helber et al. (2019). 
IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing.
