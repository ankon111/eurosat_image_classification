import os

from tensorflow.keras.losses import categorical_crossentropy
from tensorflow.keras.optimizers import Adam


class Configuration:

    def __init__(self):
        self.app_name = 'euro_sat_image_classification'
        self.working_dir = os.getcwd() + '/'
        self.data_dir = self.working_dir + 'data/'
        self.train_dir = self.data_dir + 'train'
        self.test_dir = self.data_dir + 'test'
        self.validation_dir = self.data_dir + 'validation'
        self.val_ratio = 0.08
        self.test_ratio = 0.02
        self.record_dir_name = 'records'

        # image resize config
        self.image_width = 64
        self.image_height = 64
        self.image_channel = 3

        self.class_weight_file = 'class_weights.pickle'

        # reader config
        self.record_full_path = os.path.join(self.data_dir, self.record_dir_name)
        self.seed = 1
        self.batch_size = 128
        self.train_buffer = 1000
        self.validation_buffer = 100

        # training config
        self.TF_CPP_MIN_LOG_LEVEL = 1
        self.log_dir = "log_data"
        self.epochs = 100
        self.optimizer = Adam
        self.learning_rate = 0.0001
        self.loss_func = categorical_crossentropy
        self.metrics = ["accuracy"]
        self.model_file = f"{self.app_name}_model.h5"

        #evaluation config

