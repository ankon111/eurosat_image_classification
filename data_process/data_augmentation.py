import random

import imgaug.augmenters as iaa
import tensorflow as tf

from config import Configuration


class Augmentation:

    def __init__(self, config: Configuration):
        self.config = config

    def random_jitter(self, crop):
        """ This function is used for add different augmentation to the image
        on a random basis.

        Args:
            crop: image to modify

        Returns:
            None

        """
        crop = tf.image.resize(crop, [self.config.image_width + 8, self.config.image_height + 8],
                               method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
        crop = tf.image.random_crop(crop,
                                    size=[self.config.image_width, self.config.image_width, self.config.image_channel])
        crop = tf.image.random_flip_up_down(crop)
        crop = tf.image.random_flip_left_right(crop)
        crop = tf.image.rot90(crop)
        noise = tf.random.normal(shape=tf.shape(crop), mean=0, stddev=0.02,
                                 dtype=tf.float32)
        crop = tf.add(noise, crop)

        return crop

    def aug_on_train_data(self, crop):
        """All of the training data is go through this basic augmentation process

        Args:
            crop: image to modify

        Returns:
            image
        """
        crop = crop.numpy()
        if random.random() < 0.5:
            sometimes = lambda aug: iaa.Sometimes(0.5, aug)
            seq = iaa.Sequential([sometimes(
                iaa.Affine(scale=(1.0, 1.1),
                           translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)},
                           rotate=(-20, 20),
                           shear=(-8, 8),
                           mode='edge'))])
            crop = seq.augment_image(crop)
        return crop

    def img_augmentation(self, crop, label):
        """Responsible for all kinds of image augmentation process.

        Args:
            crop: image
            label: class

        Returns:

        """
        choice = tf.random.uniform([], minval=0, maxval=1, dtype=tf.float32)
        shape = crop.get_shape()
        crop = tf.py_function(self.aug_on_train_data, inp=[crop], Tout=tf.float32)
        crop.set_shape(shape)
        crop = tf.cond(choice < 0.5, lambda: crop, lambda: self.random_jitter(crop))
        return crop, label
