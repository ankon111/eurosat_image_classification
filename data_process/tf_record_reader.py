import glob
import os

import tensorflow as tf

from config import Configuration
from data_process.data_augmentation import Augmentation
from utils.utility import Utility


class TFRecordReader:

    def __init__(self, config: Configuration):
        self.config = config
        self.augmentation = Augmentation(config=config)


    def parse_record(self, record):
        """This method is used for parsing the records e.g extracting features,
        labels.

        Args:
            record: tfrecord

        Returns:
            image, label

        """
        features = {
            'image': tf.io.FixedLenFeature([], dtype=tf.string),
            'height': tf.io.FixedLenFeature([], dtype=tf.int64),
            'width': tf.io.FixedLenFeature([], dtype=tf.int64),
            'label': tf.io.FixedLenFeature([], dtype=tf.int64),
        }
        record = tf.io.parse_single_example(record, features)
        img = tf.io.decode_raw(record['image'], tf.float32)
        img = tf.reshape(img, [record['height'], record['width'], 3])
        label = tf.one_hot(record['label'], len(Utility.get_labels()),
                           dtype=tf.float32)
        return img, label

    def generate_dataset(self, is_training=True):
        """Read tfrecord, parse the images and labels, do all the processing
        like different types of augmentation.

        Args:
            is_training: a boolean value

        Returns:
            processed dataset
        """
        batch_size = self.config.batch_size
        is_shuffle = True
        if is_training:
            dir_name = 'train'
            buffer = self.config.train_buffer
        else:
            is_shuffle = False
            dir_name = 'validation'
            buffer = self.config.validation_buffer

        files = os.path.join(self.config.record_full_path,
                             '{}_*.tf_record'.format(dir_name))
        filenames = glob.glob(files)
        dataset = tf.data.Dataset.list_files(files, shuffle=is_shuffle,
                                             seed=self.config.seed)
        dataset = dataset.interleave(lambda fn: tf.data.TFRecordDataset(fn),
                                     cycle_length=len(filenames),
                                     num_parallel_calls=min(len(filenames),
                                                            tf.data.experimental.AUTOTUNE))
        dataset = dataset.map(self.parse_record,
                              num_parallel_calls=tf.data.experimental.AUTOTUNE)
        if is_training:
            dataset = dataset.map(self.augmentation.img_augmentation,
                                  num_parallel_calls=tf.data.experimental.AUTOTUNE)
            dataset = dataset.shuffle(buffer, seed=self.config.seed)
            dataset = dataset.repeat(1)
            dataset = dataset.prefetch(1)
        else:
            dataset = dataset.repeat(1)

        dataset = dataset.batch(batch_size)

        return dataset