import os
import glob
import random
import shutil
import zipfile
from typing import List

from config import Configuration


class DataSet:

    def __init__(self, config:Configuration):
        self.config = config

    def create_dir(self):
        """ This method is responsible for creating data directory and
        removing the existing dataset.

        Returns:

        """
        if os.path.exists(self.config.data_dir):
            print("Removing existing data directory and it's content...")
            shutil.rmtree(self.config.data_dir)

        print("Creating Data directory....")
        os.mkdir(self.config.data_dir)

    def create_train_val_test_dir(self, dirs: List):
        """ This method is responsible for create train, test, validation
        directory and split the data in a given ratio

        Args:
            dirs: list of directory

        Returns:

        """
        temp_dir_name = os.path.basename(glob.glob(self.config.data_dir + '*')[0])
        classes = [os.path.basename(path) for path in
                   glob.glob(self.config.data_dir + '*/*')]

        count = len(
            glob.glob(self.config.data_dir + temp_dir_name + '/' + classes[0] + '/*'))
        val_files_count = int(self.config.val_ratio * count)
        test_files_count = int(self.config.test_ratio * count)
        train_files_count = count - val_files_count - test_files_count
        file_count = [test_files_count, val_files_count, train_files_count]
        print("Total train files count: {}".format(train_files_count))
        print("Total validation files count: {}".format(val_files_count))
        print("Total test files count: {}".format(test_files_count))

        print("File copy started......")
        for idx, dir in enumerate(dirs):
            for cls_idx, cls in enumerate(classes):
                dest_path = dir + '/' + cls
                os.makedirs(dest_path)
                src_files = glob.glob(
                    self.config.data_dir + temp_dir_name + '/' + cls + '/*.jpg')
                if idx != len(dirs) - 1:
                    src_files = random.sample(src_files, len(src_files))
                    src_files = src_files[:file_count[idx]]
                for file in src_files:
                    shutil.move(file, dest_path)

        print("File copy finished.")
        print("Removing {} directory".format(temp_dir_name))
        shutil.rmtree(self.config.data_dir + temp_dir_name)

    def remove_files(self, path: str, pattern: str):
        """ Remove files from the given path if the pattern matched

        Args:
            path: directory path
            pattern: extension pattern

        Returns:

        """
        print("Removing already extracted zip files......")
        files = glob.glob(path + pattern)
        for file in files:
            os.remove(file)

    def extract_data(self, path: str):
        """ This method is used for extracting the data.

        Args:
            path: directory path

        Returns:

        """
        files = glob.glob(path + '/*.zip')
        if files is not None or len(files) > 0:
            print("Extracting files......")
            for file in files:
                with zipfile.ZipFile(file, 'r') as zip_ref:
                    zip_ref.extractall(self.config.data_dir)