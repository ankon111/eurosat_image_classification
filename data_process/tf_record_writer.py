import glob
import os
import pickle
from collections import Counter
from typing import Tuple, List

import cv2
import numpy as np
import tensorflow as tf
# init Tf Record
from sklearn.utils import class_weight
from tqdm import tqdm

from config import Configuration
# dir_name = 'records'
# data_path = data_dir
from utils.utility import Utility


class TFRecordWriter:

    def __init__(self, config: Configuration):
        self.config = config

    def __int64_feature(self, value):
        """This functions can be used to convert a value to a type compatible
        with tf.train.Example

        Returns:
            an int64_list from int
        """
        return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

    def __bytes_feature(self, value):
        """This functions can be used to convert a value to a type compatible
        with tf.train.Example

        Returns:
            a bytes_list from a string / byte
        """
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

    def write_examples(self, example_data, filename: str, channels=3):
        """ This method is used for writing the examples as a TFRecord
        which contains features and label of the image.

        Args:
            example_data:
            filename: name of the file
            channels: value of the channels

        Returns:

        """
        with tf.io.TFRecordWriter(filename) as writer:
            for i, ex in enumerate(example_data):
                image = ex['image'].tostring()
                example = tf.train.Example(features=tf.train.Features(feature={
                    'height': self.__int64_feature(ex['image'].shape[0]),
                    'width': self.__int64_feature(ex['image'].shape[1]),
                    'depth': self.__int64_feature(channels),
                    'image': self.__bytes_feature(image),
                    'label': self.__int64_feature(Utility.encode_label(ex['label']))
                }))
                writer.write(example.SerializeToString())
        return None

    def shard_dataset(self, dataset, num_records=20) -> Tuple:
        """This method is used for getting the length of chunks and number of
        parts for this dataset.

        Args:
            dataset:
            num_records: number of records for each chunk

        Returns:

        """
        chunk = len(dataset) // num_records
        parts = [(k * chunk) for k in range(len(dataset)) if
                 (k * chunk) < len(dataset)]
        return chunk, parts

    def create_record_dir(self):
        """ This method is used for creating a directory if records directory
        doesn't exists.

        Returns:
            None
        """
        path = os.path.join(self.config.data_dir, self.config.record_dir_name)
        if not os.path.exists(path):
            os.mkdir(path)
        else:
            path = os.path.join(path, '*')
            file_list = glob.glob(path)
            for file in file_list:
                os.remove(file)

    def create_dataset(self, data_type: str) -> Tuple:
        """This function is used for create data set using metadata and
        for the train data set and it will write the class weight as this is an
        imbalance dataset.

        Args:
            data_type:

        Returns:

        """
        img_dir = os.path.join(self.config.data_dir, data_type)
        labels = os.listdir(img_dir)
        data = []
        label_list = []
        for label in tqdm(labels):
            file_names = glob.glob(os.path.join(img_dir, label, '*'))
            for file_name in file_names:
                img = cv2.imread(file_name, cv2.IMREAD_COLOR)
                if img is not None:
                    img = cv2.resize(img, (
                        self.config.image_width, self.config.image_height)) / 255
                    img = img.astype(np.float32)
                    meta = {
                        'image': img,
                        'label': label,
                        'data_type': data_type
                    }
                    data.append(meta)
                    label_list.append(label)

        """Class Weights for Imbalanced Dataset"""
        if data_type == 'train':
            self.save_class_weight(label_list=label_list)

        print('number of samples in {}: {}'.format(data_type, len(data)))
        print('data_old statistics {}'.format(
            dict(Counter(label_list))))
        return data, set(label_list)

    def save_class_weight(self, label_list: List):
        """ This method is used for saving the class weight

        Args:
            label_list: list of labels

        Returns:
            None

        """
        y = np.array(label_list)
        class_weights = class_weight.compute_class_weight(
            class_weight="balanced",
            classes=np.unique(y),
            y=y)
        class_weights = dict(enumerate(class_weights))
        print(class_weights)
        with open(self.config.class_weight_file, "wb") as file:
            pickle.dump(class_weights, file)

    def process_write_data(self, dataset: List, label:str, data_type:str):
        """This method is used for preparing the data and save the data
        as tfrecord

        Args:
            dataset: meta data
            label: label of the data
            data_type: data type e.g train or validation

        Returns:
            None

        """

        train_check = 0
        if len(dataset) > 100:
            chunk, parts = self.shard_dataset(dataset)
            for i, j in enumerate(tqdm(parts)):
                shard = dataset[j:(j + chunk)]
                file_name = '{}_{}-{}_{:03d}-{:03d}.tf_record'.format(
                    data_type,
                    label,
                    self.config.app_name,
                    i + 1,
                    len(parts))
                self.write_examples(shard,
                                    os.path.join(self.config.data_dir,
                                                 self.config.record_dir_name,
                                                 file_name))
                train_check += len(shard)
            print('Number of saved samples for {}: {}'.format(label,
                                                              train_check))
        else:
            fn = '{}_{}-{}_{:03d}-{:03d}.tf_record'.format(data_type, label,
                                                           self.config.app_name, 1, 1)
            self.write_examples(dataset,
                                os.path.join(self.config.data_dir,
                                             self.config.record_dir_name,
                                             fn))
            print('Small dataset with {} samples'.format(len(dataset)))

    def write_data(self):
        """This method is used for process and saving the data.

        Returns:
            None
        """
        for data_type in ['validation', 'train']:
            data, labels = self.create_dataset(data_type=data_type)
            for label in labels:
                label_data = [ex for ex in data if ex['label'] == label]
                self.process_write_data(label_data, label, data_type=data_type)
