import tensorflow as tf

from config import Configuration


class VGGModel:

    def __init__(self, config:Configuration):
        self.config = config

    def get_model(self):
        """ This method is creating a vgg model using functional API.

        Returns:
            model
        """
        inputs = tf.keras.Input(shape=(self.config.image_width, self.config.image_height, self.config.image_channel))
        x = inputs

        x = tf.keras.layers.Conv2D(64, 3, padding="same")(x)
        x = tf.keras.layers.ReLU()(x)
        x = tf.keras.layers.BatchNormalization()(x)

        x = tf.keras.layers.Conv2D(64, 3, padding="same")(x)
        x = tf.keras.layers.ReLU()(x)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.MaxPool2D()(x)

        x = tf.keras.layers.Conv2D(128, 3, padding="same")(x)
        x = tf.keras.layers.ReLU()(x)
        x = tf.keras.layers.BatchNormalization()(x)

        x = tf.keras.layers.Conv2D(128, 3, padding="same")(x)
        x = tf.keras.layers.ReLU()(x)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.MaxPool2D()(x)

        x = tf.keras.layers.Conv2D(256, 3, padding="same")(x)
        x = tf.keras.layers.ReLU()(x)
        x = tf.keras.layers.BatchNormalization()(x)

        x = tf.keras.layers.Conv2D(256, 3, padding="same")(x)
        x = tf.keras.layers.ReLU()(x)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.MaxPool2D()(x)

        x = tf.keras.layers.Conv2D(512, 3, padding="same")(x)
        x = tf.keras.layers.ReLU()(x)
        x = tf.keras.layers.BatchNormalization()(x)

        x = tf.keras.layers.Conv2D(512, 3, padding="same")(x)
        x = tf.keras.layers.ReLU()(x)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.MaxPool2D()(x)

        x = tf.keras.layers.GlobalAveragePooling2D(name='gap')(x)

        x = tf.keras.layers.Dense(10, activation="softmax")(x)

        base_model = tf.keras.Model(inputs, x, name='base_model')

        return base_model