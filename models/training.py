import datetime
import os
import random
import tensorflow as tf
from config import Configuration
from utils.utility import Utility


class TrainModel:

    def __init__(self, config: Configuration):
        self.config = config

        random.seed(1001)
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = f'{self.config.TF_CPP_MIN_LOG_LEVEL}'
        tf.config.experimental.list_physical_devices('GPU')

    def compile_model(self, model: tf.keras.Model):
        model.compile(optimizer=self.config.optimizer(learning_rate=self.config.learning_rate),
                      loss=self.config.loss_func, metrics=self.config.metrics)

    def train_model(self, model, train_dataset, validation_dataset):
        """This method is used for training the model.

        Return:
            None
        """
        log_dir = os.path.join(self.config.log_dir, datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))
        board = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

        history = model.fit(train_dataset, verbose=2, epochs=self.config.epochs,
                            class_weight=Utility.read_class_weight(config=self.config),
                            validation_data=validation_dataset,
                            callbacks=[board])

        train_score = model.evaluate(train_dataset, verbose=0)
        print('train loss, train acc:', train_score)

        validation_score = model.evaluate(validation_dataset, verbose=0)
        print('validation loss, validation acc:', validation_score)

        model.save(self.config.model_file)
