import glob
import os

import cv2
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from tensorflow.keras.models import load_model
from tqdm import tqdm

from config import Configuration
from utils.utility import Utility


class Evaluate:

    def __init__(self, config: Configuration):
        self.config = config

    def load_test_data(self):
        test_img_dirs = glob.glob(os.path.join(self.config.test_dir, '*'))

        x = []
        y = []

        for dir in test_img_dirs:
            label = Utility.encode_label(os.path.basename(dir))
            images = glob.glob(os.path.join(dir, '*'))
            for img in tqdm(images[:2]):
                image = cv2.imread(img, cv2.IMREAD_COLOR)
                x.append(image)
                y.append(label)

        x = np.array(x, dtype=np.float32)
        y = np.array(y, dtype=np.int16())

        return x, y

    def evaluate_model(self, x_test, y_test):
        """This method is used to evaluate the model

        Args:
            x_test: np.array
            y_test: np.array

        Returns: prediction_class

        """

        # Pixel Normalization
        x_test = x_test / 255

        model = load_model(self.config.model_file)

        # Prediction of probability and classes of test data
        prediction_probability = model.predict(x_test)
        prediction_class = prediction_probability.argmax(axis=-1)

        # Classification report includes precision, recall, and F1 score
        accuracy = accuracy_score(y_test, prediction_class)
        print('Test Dataset Accuracy Score: %f' % accuracy)

        report = classification_report(y_test, prediction_class, target_names=Utility.get_labels())
        print(report)

        # Confusion Matrix
        cm = confusion_matrix(y_test, prediction_class)
        print("Confusion Matrix:")
        print(cm)

        return prediction_class
