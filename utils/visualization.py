from typing import Dict

import scikitplot as skplt
from matplotlib import pyplot as plt


class Visualization:

    @classmethod
    def plot_dataset(cls, train_img_dict: Dict, rows: int = 2):
        """ This method is used to plot the dataset

        Args:
            train_img_dict: Dict
            rows: int

        Returns:

        """
        cols = int(len(train_img_dict) / 2)
        fig, axes = plt.subplots(rows, cols)
        fig.set_figheight(5)
        fig.set_figwidth(15)

        row = 0
        col = 0
        for idx, key in enumerate(train_img_dict):
            if idx == cols:
                row = row + 1
                col = 0
            axes[row][col].imshow(train_img_dict[key])
            axes[row][col].set_title(key)
            col = col + 1

        plt.tight_layout()
        plt.show()

    @classmethod
    def plot_tf_dataset(cls, dataset):
        """ This method is used to plot images from tf dataset

        Args:
            dataset:

        Returns:

        """
        for images, labels in dataset.take(1):  # only take first element of dataset
            numpy_images = images.numpy()
            numpy_labels = labels.numpy()

        fig, axes = plt.subplots(1, 10, squeeze=False)
        fig.set_figheight(5)
        fig.set_figwidth(20)
        row = 0
        for i in range(10):
            img = numpy_images[i][:][:][:]
            axes[row][i].imshow(img)

        plt.tight_layout()
        plt.show()

    @classmethod
    def plot_numpy_image(self, img):
        """ this method is used to plot numpy array as image

        Args:
            data: np.array

        Returns:

        """
        img = img[:, :, 0]
        plt.imshow(img)
        plt.show()

    @classmethod
    def plot_confusion_matrix(self, y_test, prediction_class):
        plot_CM = skplt.metrics.plot_confusion_matrix(y_test, prediction_class)
        plot_CM.figure.savefig("Confusion_Matrix")
        plt.show()
