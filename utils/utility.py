import pickle
from typing import List

from config import Configuration


class Utility:

    @classmethod
    def get_labels(cls) -> List:
        """This method is used to get all the labels name.

        Return:
            A list of label names.
        """
        #     labels = []
        #     for label_dir in _all_labels_dirs:
        #         labels.append(os.path.basename(label_dir))
        #     return sorted(labels)
        classes = ["AnnualCrop",
                   "Forest",
                   "HerbaceousVegetation",
                   "Highway",
                   "Industrial",
                   "Pasture",
                   "PermanentCrop",
                   "Residential",
                   "River",
                   "SeaLake"]
        return classes

    @classmethod
    def encode_label(cls, label: str) -> int:
        #     labels = get_labels()

        #     return labels.index(label)
        if label == "AnnualCrop":
            return 0
        if label == "Forest":
            return 1
        if label == 'HerbaceousVegetation':
            return 2
        if label == "Highway":
            return 3
        if label == "Industrial":
            return 4
        if label == "Pasture":
            return 5
        if label == "PermanentCrop":
            return 6
        if label == "Residential":
            return 7
        if label == "River":
            return 8
        if label == "SeaLake":
            return 9

    @classmethod
    def read_class_weight(cls, config: Configuration):
        # Reading the class weights
        class_weight_dict = None
        with open(config.class_weight_file, "rb") as data:
            class_weight_dict = pickle.load(data)

        print(class_weight_dict)

        return class_weight_dict
